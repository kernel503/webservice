package edu.ues.fia.eisi.webserviceprj2;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;



public class ControladorServicio {
    private static final String Tag = "Mensajes";

    // AQUI SE OBTIENE EL STRING EN FORMATO JSON
    public static String obtenerRespuestaPeticion(String url, Context ctx) {
        Log.i(Tag, "URL: "+url);
        String respuesta = " ";
        HttpParams parametros = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(parametros, 6000);
        HttpConnectionParams.setSoTimeout(parametros, 8000);
        HttpClient cliente = new DefaultHttpClient(parametros);
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpRespuesta = cliente.execute(httpGet);
            StatusLine estado = httpRespuesta.getStatusLine();
            int codigoEstado = estado.getStatusCode();
            if (codigoEstado == 200) {
                HttpEntity entidad = httpRespuesta.getEntity();
                respuesta = EntityUtils.toString(entidad);
            }
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en la conexion", Toast.LENGTH_LONG) .show();
            // Desplegando el error en el LogCat
            Log.v("Error de Conexion", e.toString());
        }
        return respuesta;
    }


    /*
    // SE LLENA EL ARRAY CON EL STRING JSON
    public static List<Materia> obtenerLista(String json, Context ctx) {
        List<Materia> listaMaterias = new ArrayList<Materia>();
        try {
            JSONArray JSON = new JSONArray(json);
            Log.i(Tag, "JSON A CONVERTIR: "+json);
            for (int i = 0; i < JSON.length(); i++) {
                JSONObject obj = JSON.getJSONObject(i);
                Materia materia = new Materia();
                materia.setCodMateria(obj.getString("CODMATERIA"));
                materia.setNombreMateria(obj.getString("NOMMATERIA"));
                materia.setUnidadesVal(obj.getString("UNIDADESVAL"));
                listaMaterias.add(materia);
            }
            return listaMaterias;
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en parseOO de JSON", Toast.LENGTH_LONG) .show();
            return null;
        }
    }

    */

    public static void actualizar(String peticion, Context ctx) {
        Log.i(Tag, "Actualizar: "+peticion);
        String json = obtenerRespuestaPeticion(peticion, ctx);
        Log.i(Tag, "json  "+json);
        String respuesta = "";
        try {
            JSONObject resultado = new JSONObject(json);
            respuesta = resultado.getString("resultado");
            Toast.makeText(ctx, respuesta, Toast.LENGTH_LONG) .show();
        }
        catch (JSONException e) {
            Log.i(Tag, "ERROR");
            Toast.makeText(ctx, "Error en la operacion", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void insertar(String peticion, Context ctx) {
        Log.i(Tag, "INSERTAR: "+peticion);
        String json = obtenerRespuestaPeticion(peticion, ctx);
        Log.i(Tag, "json  "+json);
        String respuesta = "";
        try {
            JSONObject resultado = new JSONObject(json);
            respuesta = resultado.getString("resultado");
            Toast.makeText(ctx, respuesta, Toast.LENGTH_LONG) .show();
        }
        catch (JSONException e) {
            Log.i(Tag, "ERROR");
            Toast.makeText(ctx, "Error en la operacion", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }



    public static void actualizarPrestamo(String peticion, Context ctx) {
        Log.i(Tag, "INSERTAR NOTA:  "+peticion);
        String json = obtenerRespuestaPeticion(peticion, ctx);
        Log.i(Tag, "json  "+json);
        int respuesta = 0;
        try {
            JSONObject resultado = new JSONObject(json);
             respuesta = resultado.getInt("resultado");
            if (respuesta == 1){
                Log.i(Tag, "ACTUALIZADO");
                Toast.makeText(ctx, "Registro Actualizado", Toast.LENGTH_LONG) .show();
            }
        }
        catch (JSONException e) {
            Log.i(Tag, "ERROR");
            Toast.makeText(ctx, "No se pudo actualizar. Revisar valores", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    public static String obtenerCantidadJSON(String json, Context ctx) {
        Log.i(Tag, "Cantidad:  "+json);
        try {
            JSONArray objs = new JSONArray(json);
            if (objs.length() != 0)
                return objs.getJSONObject(0).getString("PROMEDIO");
            else {
                Toast.makeText(ctx, "Error ID Docente no Existe", Toast.LENGTH_LONG) .show();
                return " ";
            }
        } catch (JSONException e) {
            Toast.makeText(ctx, "Error con la respuesta JSON", Toast.LENGTH_LONG).show();
            return " ";
        }
    }



    // ESTO ES PARA JAVA OMITIR

    public static String obtenerRespuestaPost(String url, JSONObject obj, Context ctx) {
        String respuesta = " ";
        try {
            HttpParams parametros = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(parametros, 3000);
            HttpConnectionParams.setSoTimeout(parametros, 5000);
            HttpClient cliente = new DefaultHttpClient(parametros);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("content-type", "application/json");
            StringEntity nuevaEntidad = new StringEntity(obj.toString());
            httpPost.setEntity(nuevaEntidad);
            Log.v("Peticion",url);
            Log.v("POST", httpPost.toString());
            HttpResponse httpRespuesta = cliente.execute(httpPost);
            StatusLine estado = httpRespuesta.getStatusLine();

            int codigoEstado = estado.getStatusCode();
            if (codigoEstado == 200) {
                respuesta = Integer.toString(codigoEstado);
                Log.v("respuesta",respuesta);
            }
            else{
                Log.v("respuesta",Integer.toString(codigoEstado));
            }
    } catch (Exception e) {
        Toast.makeText(ctx, "Error en la conexion", Toast.LENGTH_LONG) .show();
        // Desplegando el error en el LogCat
        Log.v("Error de Conexion", e.toString());
        }
        return respuesta;
    }

    public static ArrayList<oo15004DetallePrestamo> obtenerPrestamo(String json, Context ctx) {
        json = obtenerRespuestaPeticion(json, ctx);
        ArrayList<oo15004DetallePrestamo> lista = new ArrayList<oo15004DetallePrestamo>();
        try {
            JSONArray materiasJSON = new JSONArray(json);
            Log.i(Tag, "PRIMERO: "+json);
            for (int i = 0; i < materiasJSON.length(); i++) {
                JSONObject obj = materiasJSON.getJSONObject(i);
                oo15004DetallePrestamo a = new oo15004DetallePrestamo();
                a.setIdprestamo(obj.getString("idprestamo"));
                a.setIdequipo(obj.getString("idequipo"));
                a.setIddocente(obj.getString("iddocente"));
                a.setFecha(obj.getString("fecha"));
                lista.add(a);
            }
            Toast.makeText(ctx, "Registros Encoontrados: "+ lista.size(), Toast.LENGTH_LONG).show();
            return lista;
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en parseO de JSON", Toast.LENGTH_LONG) .show();
            return null;
        }
    }
    //CV15018
    public static ArrayList<cv15018DetallePrestamo> obtenerPrestamoFecha(String json, Context ctx) {
        json = obtenerRespuestaPeticion(json, ctx);
        ArrayList<cv15018DetallePrestamo> lista = new ArrayList<cv15018DetallePrestamo>();
        try {
            JSONArray materiasJSON = new JSONArray(json);
            Log.i(Tag, "PRIMERO: "+json);
            for (int i = 0; i < materiasJSON.length(); i++) {
                JSONObject obj = materiasJSON.getJSONObject(i);
                cv15018DetallePrestamo p = new cv15018DetallePrestamo();
                p.setIdprestamo(obj.getString("idprestamo"));
                p.setIdequipo(obj.getString("idequipo"));
                p.setIddocente(obj.getString("iddocente"));
                p.setFecha(obj.getString("fecha"));
                lista.add(p);
            }
            Toast.makeText(ctx, "Registros Encoontrados: "+ lista.size(), Toast.LENGTH_LONG).show();
            return lista;
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en parseO de JSON", Toast.LENGTH_LONG) .show();
            return null;
        }
    }


    public static void EliminarPrestamo(String peticion, Context ctx) {
        Log.i(Tag, "INSERTAR NOTA:  "+peticion);
        String json = obtenerRespuestaPeticion(peticion, ctx);
        Log.i(Tag, "json  "+json);
        int respuesta = 0;
        try {
            JSONObject resultado = new JSONObject(json);
            respuesta = resultado.getInt("resultado");
            if (respuesta == 1){
                Log.i(Tag, "ELIMINADO");
                Toast.makeText(ctx, "Registro Eliminado", Toast.LENGTH_LONG) .show();
            }
        }
        catch (JSONException e) {
            Log.i(Tag, "ERROR");
            Toast.makeText(ctx, "No se pudo Eliminar. Revisar valores", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    /*
    public static List<Materia> obtenerMateriasLocal(String json, Context ctx) {
        List<Materia> listaMaterias = new ArrayList<Materia>();
        try {
            JSONArray materiasJSON = new JSONArray(json);
            Log.i(Tag, "PRIMERO: "+json);
            for (int i = 0; i < materiasJSON.length(); i++) {
                JSONObject obj = materiasJSON.getJSONObject(i);
                Materia materia = new Materia();
                materia.setCodMateria(obj.getString("CODMATERIA"));
                materia.setNombreMateria(obj.getString("NOMMATERIA"));
                materia.setUnidadesVal(obj.getString("UNIDADESVAL"));
                listaMaterias.add(materia);
            }
            return listaMaterias;
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en parseO de JSON", Toast.LENGTH_LONG) .show();
            return null;
        }
    }


*/
    public static void insertarNotaLocal(String url, JSONObject obj, Context ctx) {
        String respuesta = obtenerRespuestaPost(url, obj, ctx);
        try {
            if(respuesta.equals("200"))
                Toast.makeText(ctx, "Insercion Correcta", Toast.LENGTH_LONG).show();
            else Toast.makeText(ctx, "Error registro duplicado", Toast.LENGTH_LONG).show();
            Log.v("",respuesta); }
            catch (Exception e) {
            Toast.makeText(ctx, "Error en la respuesta JSON", Toast.LENGTH_LONG).show();
        }
    }


}
