package edu.ues.fia.eisi.webserviceprj2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void actualizarPrestamo(View v){
        Intent myIntent = new Intent(this, ActualizarPrestamo.class);
        startActivity(myIntent);;
    }

    public void consultarPrestamo(View v){
        Intent myIntent = new Intent(this, ConsultarPrestamo.class);
        startActivity(myIntent);
    }

    public void insertarEquipo(View v){
        Intent myIntent = new Intent(this, InsertarEquipo.class);
        startActivity(myIntent);
    }

    public void insertarPrestamo(View v){
        Intent myIntent = new Intent(this, InsertarDetallePrestamo.class);
        startActivity(myIntent);
    }

    public void prestamoFecha(View v){
        Intent myIntent = new Intent(this, ConsultarPrestamoPorFecha.class);
        startActivity(myIntent);
    }

    public void eliminarPrestamo(View v){
        Intent myIntent = new Intent(this, EliminarPrestamo.class);
        startActivity(myIntent);
    }
}
