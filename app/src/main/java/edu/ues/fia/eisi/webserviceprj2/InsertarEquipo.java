package edu.ues.fia.eisi.webserviceprj2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InsertarEquipo extends AppCompatActivity {
    EditText id, modelo,tipo, ubicacion;
    private final String urlLocal = "http://168.232.49.193/GPO09/ws/ws_insertarEquipo.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_insertar_equipo);
        id = findViewById(R.id.insertarID);
        modelo = findViewById(R.id.insertaModelo);
        tipo = findViewById(R.id.insertarTipo);
        ubicacion = findViewById(R.id.insertarUbicacion);
    }

    public void insertarEquipo(View v){
        String url = urlLocal+"a="+id.getText().toString().replaceAll(" ", "%20")
                +"&b="+modelo.getText().toString().replaceAll(" ", "%20")
                +"&c="+tipo.getText().toString().replaceAll(" ", "%20")
                +"&d="+ubicacion.getText().toString().replaceAll(" ", "%20");
        ControladorServicio.insertar(url, this);
    }
}
