package edu.ues.fia.eisi.webserviceprj2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class EliminarPrestamo extends AppCompatActivity {
    private final String urlLocal = "http://168.232.49.193/GPO09/ws/ws_detallePrestamo_delete.php";
    EditText idPrestamo, idEquipo, idDocente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar_prestamo);



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        idPrestamo = findViewById(R.id.et_prestamo);
        idEquipo = findViewById(R.id.et_equipo);
        idDocente = findViewById(R.id.et_docente);

    }

    public void actualizar(View v){
        String url = urlLocal+"?idprestamo="+idPrestamo.getText().toString()+"&idequipo="+idEquipo.getText().toString()+"&iddocente="+idDocente.getText().toString();
        ControladorServicio.EliminarPrestamo(url, this);
        //ControladorServicio.insertar(url, this);
    }
}
