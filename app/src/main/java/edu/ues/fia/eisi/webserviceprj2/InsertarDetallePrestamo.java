package edu.ues.fia.eisi.webserviceprj2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InsertarDetallePrestamo extends AppCompatActivity {

    private final String urlLocal = "http://168.232.49.193/GPO09/ws/ws_insertarPrestamo.php?";
    EditText idPrestamo, idEquipo, idDocente, fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_detalle_prestamo);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        idPrestamo = findViewById(R.id.idPrestamo_Insertar);
        idEquipo = findViewById(R.id.idEquipo_Insertar);
        idDocente = findViewById(R.id.idDocente_Insertar);
        fecha = findViewById(R.id.idFecha_Insertar);
    }
    public void insertarPrestamo(View v){
        String url = urlLocal
                +"a="+idPrestamo.getText().toString().replaceAll(" ", "%20")
                +"&b="+idEquipo.getText().toString().replaceAll(" ", "%20")
                +"&c="+idDocente.getText().toString().replaceAll(" ", "%20")
                +"&d="+fecha.getText().toString().replaceAll(" ", "%20");
        ControladorServicio.insertar(url, this);
    }

}
