package edu.ues.fia.eisi.webserviceprj2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ActualizarPrestamo extends AppCompatActivity {

    private final String urlLocal = "http://168.232.49.193/GPO09/ws/ws_actualizarPrestamo.php?";
    EditText idPrestamo, idEquipo, idDocente, fecha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_prestamo);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        idPrestamo = findViewById(R.id.idPrestamo_Actualizar);
        idEquipo = findViewById(R.id.idEquipo_Actualizar);
        idDocente = findViewById(R.id.idDocente_Actualizar);
        fecha = findViewById(R.id.idFecha_Actualizar);
    }

    public void actualizar(View v){
        String url = urlLocal+"prestamo="+idPrestamo.getText().toString()
                +"&equipo="+idEquipo.getText().toString()+"&docente="+idDocente.getText().toString()
                +"&fecha="+fecha.getText().toString();
        ControladorServicio.actualizar(url, this);
    }

}
