package edu.ues.fia.eisi.webserviceprj2;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class ControlBD {

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private static final String Tag = "Mensajes";

    public ControlBD(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "dbPrestamo.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{
                db.execSQL("CREATE TABLE detallePrestamo (idPrestamo VARCHAR(5) NOT NULL PRIMARY KEY, " +
                        "idequipo VARCHAR(5) NOT NULL , iddocente VARCHAR(5) NOT NULL, fecha VARCHAR(30) NOT NULL );");
                Log.i(Tag, "SE CREO EXITOSAMENTE");
            }catch(SQLException e){
                e.printStackTrace();
                Log.i(Tag, "NO SE CREO");
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }
    }

    public void abrir() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar(){
        DBHelper.close();
    }

    public String insertarInterno(ArrayList<oo15004DetallePrestamo> datos){
        abrir();
        String s = "";
        String[] args = new String[4];
        int regAfectados = 0;
        for (oo15004DetallePrestamo a : datos){
            args[0]=a.getIdprestamo();
            args[1]=a.getIdequipo();
            args[2]=a.getIddocente();
            args[3]=a.getFecha();
            try{
                db.execSQL("INSERT INTO detallePrestamo VALUES (?,?,?,?);",args);
                regAfectados++;
            }catch (SQLException ex){
                s = "Registro ya ingresados";
            }
            if (regAfectados==0){
                s = "Datos existentes";
            }else{
                s = "Registro ingresados: "+regAfectados;
            }
        }
        cerrar();
        return s;
    }
}

