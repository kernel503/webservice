package edu.ues.fia.eisi.webserviceprj2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class ConsultarPrestamoPorFecha extends AppCompatActivity {
    EditText fecha;
    ListView listView;

    ArrayList<cv15018DetallePrestamo> lista;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private final String  urlLocal = "http://168.232.49.193/GPO09/ws/ws_detallePrestamo_fecha.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_prestamo_por_fecha);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        fecha = findViewById(R.id.et_fechaCV15018);
        listView = findViewById(R.id.listaFechaCV15018);
    }


    public void consultarPrestamoFecha(View v){
        String[] date = fecha.getText().toString().split("/");
        String url = urlLocal+"?year="+date[0]+"&month="+date[1]+"&day="+date[2];
        lista = ControladorServicio.obtenerPrestamoFecha(url, this);
        llenarLista();
    }

    private void llenarLista(){
        stringArrayList= new ArrayList<>();
        if (!lista.isEmpty()){
            for (cv15018DetallePrestamo a : lista){
                stringArrayList.add(a.getIdprestamo()+" "+a.getIdequipo()+"\n"+" "+a.getIddocente()+"\n"+a.getFecha());
            }
        }else {
            stringArrayList.add("Lista Vacia");
        }
        stringArrayAdapter =new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

}
