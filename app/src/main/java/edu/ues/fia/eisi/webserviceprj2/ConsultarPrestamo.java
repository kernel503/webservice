package edu.ues.fia.eisi.webserviceprj2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ConsultarPrestamo extends AppCompatActivity {
    EditText idDocente;
    ListView listView;
    ControlBD BDhelper;

    ArrayList<oo15004DetallePrestamo> lista;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;


    private final String urlLocal = "http://168.232.49.193/GPO09/ws/ws_consultarPrestamo.php?";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_prestamo);

        BDhelper=new ControlBD(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        idDocente = findViewById(R.id.idDocente_Consultar);
        listView = findViewById(R.id.listaPrestamo);
    }

    public void consultarDocente(View v){
        String url = urlLocal+"docente="+idDocente.getText().toString();
        lista = ControladorServicio.obtenerPrestamo(url, this);
        llenarTabla();
    }

    public void guardar(View v){
        Toast.makeText(this,BDhelper.insertarInterno(lista),Toast.LENGTH_SHORT).show();
        lista.clear();
        llenarTabla();
    }

    private void llenarTabla(){
        stringArrayList= new ArrayList<>();
        if (!lista.isEmpty()){
            for (oo15004DetallePrestamo a : lista){
                stringArrayList.add(a.getIdprestamo()+" "+a.getIdequipo()+"\n"+a.getFecha());
            }
        }else {
            stringArrayList.add("Lista Vacia");
        }
        stringArrayAdapter =new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

}
