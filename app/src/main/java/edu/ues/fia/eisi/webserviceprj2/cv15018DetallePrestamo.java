package edu.ues.fia.eisi.webserviceprj2;

public class cv15018DetallePrestamo {
    String idprestamo, idequipo, iddocente,fecha;

    public cv15018DetallePrestamo() {
    }

    public cv15018DetallePrestamo(String idprestamo, String idequipo, String iddocente, String fecha) {
        this.idprestamo = idprestamo;
        this.idequipo = idequipo;
        this.iddocente = iddocente;
        this.fecha = fecha;
    }

    public String getIdprestamo() {
        return idprestamo;
    }

    public void setIdprestamo(String idprestamo) {
        this.idprestamo = idprestamo;
    }

    public String getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(String idequipo) {
        this.idequipo = idequipo;
    }

    public String getIddocente() {
        return iddocente;
    }

    public void setIddocente(String iddocente) {
        this.iddocente = iddocente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
